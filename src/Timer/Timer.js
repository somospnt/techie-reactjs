import React, { Component } from 'react';

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {segundos: 0};
    }

    tick() {
        this.setState((prevState) => ({
                segundos: prevState.segundos + 1
            }));
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
    }

    render() {
        return (
                <div>
                    Segundos: 
                </div>
                );
    }
}

export default Timer;
