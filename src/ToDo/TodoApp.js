import React, { Component } from 'react';
import ListaToDo from './ListaToDo';

class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {items: [], text: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({text: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const newItem = {
            text: this.state.text
        };
        this.setState((prevState) => ({
                items: prevState.items.concat(newItem),
                text: ''
            }));
    }

    render() {
        return (
                <div>
                    <h3>TODO</h3>
                    <form>
                        <input/>
                        <button>
                            Add #
                        </button>
                    </form>
                </div>
                );
    }
}

export default TodoApp;
