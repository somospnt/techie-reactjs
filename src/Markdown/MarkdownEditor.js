import React, { Component } from 'react';
import Remarkable from 'remarkable';

class MarkdownEditor extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = { value: '# Hola Mundo \n ## Soy un ejemplo en React \n ### O como diria Miller... \n #### Hola soy Miller!' };
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  getRawMarkup() {
    const md = new Remarkable();
    return { __html: md.render(this.state.value) };
  }

  render() {
    return (
      <div className="MarkdownEditor">
        <h3>Input</h3>
        <textarea/>
        <h3>Output</h3>
        <div
          className="content"
        />
      </div>
    );
  }
}

export default MarkdownEditor;
