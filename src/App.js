import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoApp from './ToDo/TodoApp';
import Timer from './Timer/Timer';
import MarkdownEditor from './Markdown/MarkdownEditor';

class App extends Component {
    render() {
        return (
                <div>
                    <div className="App">
                        <header className="App-header">
                            <img src={logo} className="App-logo" alt="logo" />
                            <h1 className="App-title">Welcome to React</h1>
                        </header>
                    </div>
                    <br/>
                    Timer
                    <Timer />
                    <br/>
                    Lista de ToDo
                    <TodoApp />
                    <br/>
                    Editor Markdown
                    <MarkdownEditor />
                </div>
                );
    }
}

export default App;
